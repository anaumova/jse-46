package ru.tsc.anaumova.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.anaumova.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public IDomainEndpoint getDomainEndpoint() {
        return serviceLocator.getDomainEndpoint();
    }

}