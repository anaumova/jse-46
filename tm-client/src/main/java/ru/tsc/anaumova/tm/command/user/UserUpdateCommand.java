package ru.tsc.anaumova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.dto.request.UserUpdateRequest;
import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.util.TerminalUtil;

public final class UserUpdateCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-update";

    @NotNull
    public static final String DESCRIPTION = "Update user";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER FIRST NAME:]");
        @NotNull String firstName = TerminalUtil.nextLine();
        System.out.println("[ENTER LAST NAME:]");
        @NotNull String lastName = TerminalUtil.nextLine();
        System.out.println("[ENTER MIDDLE NAME:]");
        @NotNull String middleName = TerminalUtil.nextLine();
        getUserEndpoint().updateUser(new UserUpdateRequest(getToken(), firstName, lastName, middleName));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}