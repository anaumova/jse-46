package ru.tsc.anaumova.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataJsonSaveJaxbRequest extends AbstractUserRequest {

    public DataJsonSaveJaxbRequest(@Nullable String token) {
        super(token);
    }

}