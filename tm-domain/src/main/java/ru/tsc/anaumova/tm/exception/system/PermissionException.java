package ru.tsc.anaumova.tm.exception.system;

import ru.tsc.anaumova.tm.exception.AbstractException;

public final class PermissionException extends AbstractException {

    public PermissionException() {
        super("Error! Permission is incorrect...");
    }

}